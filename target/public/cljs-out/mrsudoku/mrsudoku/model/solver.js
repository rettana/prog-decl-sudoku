// Compiled by ClojureScript 1.10.339 {}
goog.provide('mrsudoku.model.solver');
goog.require('cljs.core');
goog.require('mrsudoku.model.grid');
goog.require('mrsudoku.model.conflict');
mrsudoku.model.solver.VALUE = new cljs.core.Keyword(null,"value","value",305978217);
mrsudoku.model.solver.COL = new cljs.core.Keyword(null,"col","col",-1959363084);
mrsudoku.model.solver.ROW = new cljs.core.Keyword(null,"row","row",-570139521);
mrsudoku.model.solver.BLOCK = new cljs.core.Keyword(null,"block","block",664686210);
mrsudoku.model.solver.getDomain = (function mrsudoku$model$solver$getDomain(grid,type,idOfDomain){
if(cljs.core._EQ_.call(null,type,mrsudoku.model.solver.COL)){
return mrsudoku.model.grid.col.call(null,grid,idOfDomain);
} else {
if(cljs.core._EQ_.call(null,type,mrsudoku.model.solver.ROW)){
return mrsudoku.model.grid.row.call(null,grid,idOfDomain);
} else {
if(cljs.core._EQ_.call(null,type,mrsudoku.model.solver.BLOCK)){
return mrsudoku.model.grid.block.call(null,grid,idOfDomain);
} else {
return null;
}
}
}
});
mrsudoku.model.solver.getPossiblesValuesOfDomain = (function mrsudoku$model$solver$getPossiblesValuesOfDomain(grid,type,idOfDomain){

mrsudoku.model.solver.allValues = cljs.core.set.call(null,(function (){var iter__4324__auto__ = (function mrsudoku$model$solver$getPossiblesValuesOfDomain_$_iter__10329(s__10330){
return (new cljs.core.LazySeq(null,(function (){
var s__10330__$1 = s__10330;
while(true){
var temp__5720__auto__ = cljs.core.seq.call(null,s__10330__$1);
if(temp__5720__auto__){
var s__10330__$2 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10330__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__10330__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__10332 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__10331 = (0);
while(true){
if((i__10331 < size__4323__auto__)){
var i = cljs.core._nth.call(null,c__4322__auto__,i__10331);
cljs.core.chunk_append.call(null,b__10332,i);

var G__10333 = (i__10331 + (1));
i__10331 = G__10333;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10332),mrsudoku$model$solver$getPossiblesValuesOfDomain_$_iter__10329.call(null,cljs.core.chunk_rest.call(null,s__10330__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10332),null);
}
} else {
var i = cljs.core.first.call(null,s__10330__$2);
return cljs.core.cons.call(null,i,mrsudoku$model$solver$getPossiblesValuesOfDomain_$_iter__10329.call(null,cljs.core.rest.call(null,s__10330__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,cljs.core.range.call(null,(1),(10)));
})());

mrsudoku.model.solver.valuesOfDomain = cljs.core.set.call(null,cljs.core.remove.call(null,cljs.core.nil_QMARK_,cljs.core.reduce.call(null,(function (res,x){
return cljs.core.conj.call(null,res,cljs.core.get.call(null,x,mrsudoku.model.solver.VALUE));
}),cljs.core.PersistentHashSet.EMPTY,mrsudoku.model.solver.getDomain.call(null,grid,type,idOfDomain))));

return clojure.set.difference.call(null,mrsudoku.model.solver.allValues,mrsudoku.model.solver.valuesOfDomain);
});
mrsudoku.model.solver.getEmptyCasesIndexOfDomain = (function mrsudoku$model$solver$getEmptyCasesIndexOfDomain(grid,type,idOfDomain){
return cljs.core.set.call(null,cljs.core.remove.call(null,cljs.core.nil_QMARK_,(function (){var iter__4324__auto__ = (function mrsudoku$model$solver$getEmptyCasesIndexOfDomain_$_iter__10334(s__10335){
return (new cljs.core.LazySeq(null,(function (){
var s__10335__$1 = s__10335;
while(true){
var temp__5720__auto__ = cljs.core.seq.call(null,s__10335__$1);
if(temp__5720__auto__){
var s__10335__$2 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10335__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__10335__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__10337 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__10336 = (0);
while(true){
if((i__10336 < size__4323__auto__)){
var i = cljs.core._nth.call(null,c__4322__auto__,i__10336);
cljs.core.chunk_append.call(null,b__10337,((cljs.core._EQ_.call(null,cljs.core.get.call(null,cljs.core.get.call(null,mrsudoku.model.solver.getDomain.call(null,grid,type,idOfDomain),i),new cljs.core.Keyword(null,"value","value",305978217)),null))?(i + (1)):null));

var G__10338 = (i__10336 + (1));
i__10336 = G__10338;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10337),mrsudoku$model$solver$getEmptyCasesIndexOfDomain_$_iter__10334.call(null,cljs.core.chunk_rest.call(null,s__10335__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10337),null);
}
} else {
var i = cljs.core.first.call(null,s__10335__$2);
return cljs.core.cons.call(null,((cljs.core._EQ_.call(null,cljs.core.get.call(null,cljs.core.get.call(null,mrsudoku.model.solver.getDomain.call(null,grid,type,idOfDomain),i),new cljs.core.Keyword(null,"value","value",305978217)),null))?(i + (1)):null),mrsudoku$model$solver$getEmptyCasesIndexOfDomain_$_iter__10334.call(null,cljs.core.rest.call(null,s__10335__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,cljs.core.range.call(null,(0),(9)));
})()));
});
mrsudoku.model.solver.getCaseIdOfTab = (function mrsudoku$model$solver$getCaseIdOfTab(type,idOfDomain,idInDomain){
mrsudoku.model.solver.p = (idOfDomain - (1));

mrsudoku.model.solver.i = (idInDomain - (1));

if(cljs.core._EQ_.call(null,type,mrsudoku.model.solver.ROW)){
return ((mrsudoku.model.solver.p * (9)) + mrsudoku.model.solver.i);
} else {
if(cljs.core._EQ_.call(null,type,mrsudoku.model.solver.COL)){
return ((mrsudoku.model.solver.p * (9)) + mrsudoku.model.solver.i);
} else {
if(cljs.core._EQ_.call(null,type,mrsudoku.model.solver.BLOCK)){
return (((((3) * cljs.core.rem.call(null,mrsudoku.model.solver.p,(3))) + ((27) * cljs.core.quot.call(null,mrsudoku.model.solver.p,(3)))) + cljs.core.rem.call(null,mrsudoku.model.solver.i,(3))) + ((9) * cljs.core.quot.call(null,mrsudoku.model.solver.i,(3))));
} else {
return null;
}
}
}
});
mrsudoku.model.solver.getCasePositionFromIdOfTab = (function mrsudoku$model$solver$getCasePositionFromIdOfTab(idOfTab){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [((1) + cljs.core.rem.call(null,idOfTab,(9))),((1) + cljs.core.quot.call(null,idOfTab,(9)))], null);
});
mrsudoku.model.solver.getCasePositionFromDomainIds = (function mrsudoku$model$solver$getCasePositionFromDomainIds(type,idOfDomain,idInDomain){
return mrsudoku.model.solver.getCasePositionFromIdOfTab.call(null,mrsudoku.model.solver.getCaseIdOfTab.call(null,type,idOfDomain,idInDomain));
});
mrsudoku.model.solver.getBlockFromPosition = (function mrsudoku$model$solver$getBlockFromPosition(x,y){
return (((1) + ((3) * cljs.core.quot.call(null,(y - (1)),(3)))) + cljs.core.quot.call(null,(x - (1)),(3)));
});
mrsudoku.model.solver.changeOneValueInGrid = (function mrsudoku$model$solver$changeOneValueInGrid(grid,x,y,value){
mrsudoku.model.solver.newCell = cljs.core.assoc.call(null,cljs.core.assoc.call(null,mrsudoku.model.grid.cell.call(null,grid,x,y),new cljs.core.Keyword(null,"value","value",305978217),value),new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"set","set",304602554));

return mrsudoku.model.grid.change_cell.call(null,grid,x,y,mrsudoku.model.solver.newCell);
});
mrsudoku.model.solver.getPossiblesValuesOfCase = (function mrsudoku$model$solver$getPossiblesValuesOfCase(grid,possibleValues,type,idOfDomain,idInDomain){
return cljs.core.PersistentArrayMap.createAsIfByAssoc([idInDomain,cljs.core.set.call(null,cljs.core.remove.call(null,cljs.core.nil_QMARK_,(function (){var iter__4324__auto__ = (function mrsudoku$model$solver$getPossiblesValuesOfCase_$_iter__10339(s__10340){
return (new cljs.core.LazySeq(null,(function (){
var s__10340__$1 = s__10340;
while(true){
var temp__5720__auto__ = cljs.core.seq.call(null,s__10340__$1);
if(temp__5720__auto__){
var s__10340__$2 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10340__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__10340__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__10342 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__10341 = (0);
while(true){
if((i__10341 < size__4323__auto__)){
var v = cljs.core._nth.call(null,c__4322__auto__,i__10341);
cljs.core.chunk_append.call(null,b__10342,(function (){
mrsudoku.model.solver.posXY = mrsudoku.model.solver.getCasePositionFromDomainIds.call(null,type,idOfDomain,idInDomain);

mrsudoku.model.solver.x = cljs.core.get.call(null,mrsudoku.model.solver.posXY,(0));

mrsudoku.model.solver.y = cljs.core.get.call(null,mrsudoku.model.solver.posXY,(1));

mrsudoku.model.solver.tmpGrid = mrsudoku.model.solver.changeOneValueInGrid.call(null,grid,mrsudoku.model.solver.x,mrsudoku.model.solver.y,v);

mrsudoku.model.solver.tmpCol = mrsudoku.model.grid.col.call(null,mrsudoku.model.solver.tmpGrid,mrsudoku.model.solver.x);

mrsudoku.model.solver.tmpRow = mrsudoku.model.grid.row.call(null,mrsudoku.model.solver.tmpGrid,mrsudoku.model.solver.y);

mrsudoku.model.solver.tmpBlock = mrsudoku.model.grid.block.call(null,mrsudoku.model.solver.tmpGrid,mrsudoku.model.solver.getBlockFromPosition.call(null,mrsudoku.model.solver.x,mrsudoku.model.solver.y));

if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,mrsudoku.model.conflict.block_conflicts.call(null,mrsudoku.model.solver.tmpBlock,v))){
if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,mrsudoku.model.conflict.row_conflicts.call(null,mrsudoku.model.solver.tmpRow,v))){
if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,mrsudoku.model.conflict.col_conflicts.call(null,mrsudoku.model.solver.tmpCol,v))){
return v;
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})()
);

var G__10343 = (i__10341 + (1));
i__10341 = G__10343;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10342),mrsudoku$model$solver$getPossiblesValuesOfCase_$_iter__10339.call(null,cljs.core.chunk_rest.call(null,s__10340__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10342),null);
}
} else {
var v = cljs.core.first.call(null,s__10340__$2);
return cljs.core.cons.call(null,(function (){
mrsudoku.model.solver.posXY = mrsudoku.model.solver.getCasePositionFromDomainIds.call(null,type,idOfDomain,idInDomain);

mrsudoku.model.solver.x = cljs.core.get.call(null,mrsudoku.model.solver.posXY,(0));

mrsudoku.model.solver.y = cljs.core.get.call(null,mrsudoku.model.solver.posXY,(1));

mrsudoku.model.solver.tmpGrid = mrsudoku.model.solver.changeOneValueInGrid.call(null,grid,mrsudoku.model.solver.x,mrsudoku.model.solver.y,v);

mrsudoku.model.solver.tmpCol = mrsudoku.model.grid.col.call(null,mrsudoku.model.solver.tmpGrid,mrsudoku.model.solver.x);

mrsudoku.model.solver.tmpRow = mrsudoku.model.grid.row.call(null,mrsudoku.model.solver.tmpGrid,mrsudoku.model.solver.y);

mrsudoku.model.solver.tmpBlock = mrsudoku.model.grid.block.call(null,mrsudoku.model.solver.tmpGrid,mrsudoku.model.solver.getBlockFromPosition.call(null,mrsudoku.model.solver.x,mrsudoku.model.solver.y));

if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,mrsudoku.model.conflict.block_conflicts.call(null,mrsudoku.model.solver.tmpBlock,v))){
if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,mrsudoku.model.conflict.row_conflicts.call(null,mrsudoku.model.solver.tmpRow,v))){
if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,mrsudoku.model.conflict.col_conflicts.call(null,mrsudoku.model.solver.tmpCol,v))){
return v;
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})()
,mrsudoku$model$solver$getPossiblesValuesOfCase_$_iter__10339.call(null,cljs.core.rest.call(null,s__10340__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,possibleValues);
})()))]);
});
mrsudoku.model.solver.getPossiblesCaseOfValues = (function mrsudoku$model$solver$getPossiblesCaseOfValues(setOfPossiblesValuesOfCase,idInDomain){
return cljs.core.reduce.call(null,(function (res,v){
return cljs.core.assoc.call(null,res,v,cljs.core.PersistentHashSet.createAsIfByAssoc([idInDomain]));
}),cljs.core.PersistentArrayMap.EMPTY,setOfPossiblesValuesOfCase);
});
mrsudoku.model.solver.solveOneCase = (function mrsudoku$model$solver$solveOneCase(domainVals,caseOFValues,idCase){
return cljs.core.reduce.call(null,(function (minIndex,v){
if((cljs.core.count.call(null,cljs.core.get.call(null,caseOFValues,v)) < ((cljs.core._EQ_.call(null,minIndex,(10)))?(10):cljs.core.count.call(null,cljs.core.get.call(null,caseOFValues,minIndex))))){
return v;
} else {
return minIndex;
}
}),(10),cljs.core.vec.call(null,cljs.core.get.call(null,domainVals,idCase)));
});
mrsudoku.model.solver.IndexOfCaseWithMinimalSize = (function mrsudoku$model$solver$IndexOfCaseWithMinimalSize(domainVals){
return cljs.core.reduce.call(null,(function (res,i){
if((cljs.core.count.call(null,cljs.core.get.call(null,i,(1))) < ((cljs.core._EQ_.call(null,res,(10)))?(10):cljs.core.count.call(null,cljs.core.get.call(null,domainVals,res))))){
return cljs.core.get.call(null,i,(0));
} else {
return res;
}
}),(10),domainVals);
});
mrsudoku.model.solver.buildLinkGraphOfDomain = (function mrsudoku$model$solver$buildLinkGraphOfDomain(grid,type,idOfDomain){
mrsudoku.model.solver._possiblesVals = mrsudoku.model.solver.getPossiblesValuesOfDomain.call(null,grid,type,idOfDomain);

mrsudoku.model.solver._emptyCases = mrsudoku.model.solver.getEmptyCasesIndexOfDomain.call(null,grid,type,idOfDomain);

mrsudoku.model.solver._domainVals = cljs.core.reduce.call(null,(function (res,c){
return cljs.core.conj.call(null,res,mrsudoku.model.solver.getPossiblesValuesOfCase.call(null,grid,mrsudoku.model.solver._possiblesVals,type,idOfDomain,c));
}),cljs.core.PersistentArrayMap.EMPTY,mrsudoku.model.solver._emptyCases);

mrsudoku.model.solver._casesOfValues = cljs.core.reduce.call(null,(function (res,p){
return cljs.core.merge_with.call(null,cljs.core.into,res,p);
}),cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4324__auto__ = (function mrsudoku$model$solver$buildLinkGraphOfDomain_$_iter__10344(s__10345){
return (new cljs.core.LazySeq(null,(function (){
var s__10345__$1 = s__10345;
while(true){
var temp__5720__auto__ = cljs.core.seq.call(null,s__10345__$1);
if(temp__5720__auto__){
var s__10345__$2 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10345__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__10345__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__10347 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__10346 = (0);
while(true){
if((i__10346 < size__4323__auto__)){
var i = cljs.core._nth.call(null,c__4322__auto__,i__10346);
cljs.core.chunk_append.call(null,b__10347,mrsudoku.model.solver.getPossiblesCaseOfValues.call(null,cljs.core.get.call(null,mrsudoku.model.solver._domainVals,i),i));

var G__10348 = (i__10346 + (1));
i__10346 = G__10348;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10347),mrsudoku$model$solver$buildLinkGraphOfDomain_$_iter__10344.call(null,cljs.core.chunk_rest.call(null,s__10345__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10347),null);
}
} else {
var i = cljs.core.first.call(null,s__10345__$2);
return cljs.core.cons.call(null,mrsudoku.model.solver.getPossiblesCaseOfValues.call(null,cljs.core.get.call(null,mrsudoku.model.solver._domainVals,i),i),mrsudoku$model$solver$buildLinkGraphOfDomain_$_iter__10344.call(null,cljs.core.rest.call(null,s__10345__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,cljs.core.keys.call(null,mrsudoku.model.solver._domainVals));
})());

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [mrsudoku.model.solver._domainVals,mrsudoku.model.solver._casesOfValues], null);
});
mrsudoku.model.solver.removeOneCoupleOfLinkGraph = (function mrsudoku$model$solver$removeOneCoupleOfLinkGraph(linkGraph,idCase,idVal){
mrsudoku.model.solver.tmpCase = cljs.core.dissoc.call(null,cljs.core.reduce.call(null,(function (res,k){
return cljs.core.assoc.call(null,res,k,cljs.core.disj.call(null,cljs.core.get.call(null,res,k),idCase));
}),cljs.core.get.call(null,linkGraph,(1)),cljs.core.keys.call(null,cljs.core.get.call(null,linkGraph,(1)))),idVal);

mrsudoku.model.solver.tmpVal = cljs.core.dissoc.call(null,cljs.core.reduce.call(null,(function (res,k){
return cljs.core.assoc.call(null,res,k,cljs.core.disj.call(null,cljs.core.get.call(null,res,k),idVal));
}),cljs.core.get.call(null,linkGraph,(0)),cljs.core.keys.call(null,cljs.core.get.call(null,linkGraph,(0)))),idCase);

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [mrsudoku.model.solver.tmpVal,mrsudoku.model.solver.tmpCase], null);
});
mrsudoku.model.solver.solveLinkGraph = (function mrsudoku$model$solver$solveLinkGraph(linkGraph,res){
if(cljs.core.not_EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,cljs.core.get.call(null,linkGraph,(0)))){
mrsudoku.model.solver.minI = mrsudoku.model.solver.IndexOfCaseWithMinimalSize.call(null,cljs.core.get.call(null,linkGraph,(0)));

mrsudoku.model.solver.subRes = mrsudoku.model.solver.solveOneCase.call(null,cljs.core.get.call(null,linkGraph,(0)),cljs.core.get.call(null,linkGraph,(1)),mrsudoku.model.solver.minI);

return cljs.core.merge.call(null,cljs.core.assoc.call(null,res,mrsudoku.model.solver.minI,mrsudoku.model.solver.subRes),mrsudoku.model.solver.solveLinkGraph.call(null,mrsudoku.model.solver.removeOneCoupleOfLinkGraph.call(null,linkGraph,mrsudoku.model.solver.minI,mrsudoku.model.solver.subRes),res));
} else {
return res;
}
});
mrsudoku.model.solver.removeEdgesFromMap = (function mrsudoku$model$solver$removeEdgesFromMap(linkGraph,map){
return cljs.core.reduce.call(null,(function (res,m){
mrsudoku.model.solver.l = cljs.core.get.call(null,m,(0));

mrsudoku.model.solver.r = cljs.core.get.call(null,m,(1));

mrsudoku.model.solver.__vals = cljs.core.get.call(null,res,(0));

mrsudoku.model.solver.__cases = cljs.core.get.call(null,res,(1));

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [((cljs.core._EQ_.call(null,(1),cljs.core.count.call(null,cljs.core.get.call(null,mrsudoku.model.solver.__vals,mrsudoku.model.solver.l))))?mrsudoku.model.solver.__vals:cljs.core.assoc.call(null,mrsudoku.model.solver.__vals,mrsudoku.model.solver.l,cljs.core.disj.call(null,cljs.core.get.call(null,mrsudoku.model.solver.__vals,mrsudoku.model.solver.l),mrsudoku.model.solver.r))),((cljs.core._EQ_.call(null,(1),cljs.core.count.call(null,cljs.core.get.call(null,mrsudoku.model.solver.__cases,mrsudoku.model.solver.r))))?mrsudoku.model.solver.__cases:cljs.core.assoc.call(null,mrsudoku.model.solver.__cases,mrsudoku.model.solver.r,cljs.core.disj.call(null,cljs.core.get.call(null,mrsudoku.model.solver.__cases,mrsudoku.model.solver.r),mrsudoku.model.solver.l)))], null);
}),linkGraph,map);
});
mrsudoku.model.solver.removeOneEdgeFromMap = (function mrsudoku$model$solver$removeOneEdgeFromMap(linkGraph,map){
if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,map)){
return null;
} else {
mrsudoku.model.solver.vMap = cljs.core.get.call(null,cljs.core.vec.call(null,map),(0));

mrsudoku.model.solver.___l = cljs.core.get.call(null,mrsudoku.model.solver.vMap,(0));

mrsudoku.model.solver.___r = cljs.core.get.call(null,mrsudoku.model.solver.vMap,(1));

mrsudoku.model.solver.___vals = cljs.core.get.call(null,linkGraph,(0));

mrsudoku.model.solver.___cases = cljs.core.get.call(null,linkGraph,(1));

if(((cljs.core._EQ_.call(null,(1),cljs.core.count.call(null,cljs.core.get.call(null,mrsudoku.model.solver.___vals,mrsudoku.model.solver.___l)))) || (cljs.core._EQ_.call(null,(1),cljs.core.count.call(null,cljs.core.get.call(null,mrsudoku.model.solver.___cases,mrsudoku.model.solver.___r)))))){
return mrsudoku.model.solver.removeOneEdgeFromMap.call(null,linkGraph,cljs.core.dissoc.call(null,map,mrsudoku.model.solver.___l));
} else {
return mrsudoku.model.solver.removeEdgesFromMap.call(null,linkGraph,cljs.core.PersistentArrayMap.createAsIfByAssoc([mrsudoku.model.solver.___l,mrsudoku.model.solver.___r]));
}
}
});
mrsudoku.model.solver.maximalMatchingOfDomain = (function mrsudoku$model$solver$maximalMatchingOfDomain(linkGraph){
mrsudoku.model.solver._vals = cljs.core.get.call(null,linkGraph,(0));

mrsudoku.model.solver._cases = cljs.core.get.call(null,linkGraph,(1));

return mrsudoku.model.solver.solveLinkGraph.call(null,linkGraph,cljs.core.PersistentArrayMap.EMPTY);
});
mrsudoku.model.solver.keyAndSetToMap = (function mrsudoku$model$solver$keyAndSetToMap(keys,_set){
return cljs.core.reduce.call(null,(function (res,i){
return cljs.core.assoc.call(null,res,cljs.core.get.call(null,keys,i),cljs.core.get.call(null,cljs.core.vec.call(null,_set),i));
}),null,cljs.core.range.call(null,cljs.core.count.call(null,keys)));
});
mrsudoku.model.solver.maximalMatchingRecursive = (function mrsudoku$model$solver$maximalMatchingRecursive(res,stack,values,valuesSize,test){
if(cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,values)){
if(cljs.core._EQ_.call(null,cljs.core.count.call(null,stack),valuesSize)){
return cljs.core.conj.call(null,res,test);
} else {
return null;
}
} else {
mrsudoku.model.solver.vIndex = cljs.core.get.call(null,cljs.core.get.call(null,cljs.core.vec.call(null,values),(0)),(0));

mrsudoku.model.solver.vSet = cljs.core.get.call(null,values,mrsudoku.model.solver.vIndex);

var iter__4324__auto__ = (function mrsudoku$model$solver$maximalMatchingRecursive_$_iter__10349(s__10350){
return (new cljs.core.LazySeq(null,(function (){
var s__10350__$1 = s__10350;
while(true){
var temp__5720__auto__ = cljs.core.seq.call(null,s__10350__$1);
if(temp__5720__auto__){
var s__10350__$2 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__10350__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__10350__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__10352 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__10351 = (0);
while(true){
if((i__10351 < size__4323__auto__)){
var v = cljs.core._nth.call(null,c__4322__auto__,i__10351);
cljs.core.chunk_append.call(null,b__10352,(function (){
mrsudoku.model.solver.newStack = cljs.core.conj.call(null,stack,v);

if(cljs.core._EQ_.call(null,mrsudoku.model.solver.vIndex,(3))){
cljs.core.println.call(null,v,values);
} else {
}

return cljs.core.remove.call(null,cljs.core.nil_QMARK_,mrsudoku.model.solver.maximalMatchingRecursive.call(null,res,mrsudoku.model.solver.newStack,cljs.core.dissoc.call(null,values,mrsudoku.model.solver.vIndex),valuesSize,cljs.core.conj.call(null,test,cljs.core.PersistentArrayMap.createAsIfByAssoc([mrsudoku.model.solver.vIndex,v]))));
})()
);

var G__10353 = (i__10351 + (1));
i__10351 = G__10353;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10352),mrsudoku$model$solver$maximalMatchingRecursive_$_iter__10349.call(null,cljs.core.chunk_rest.call(null,s__10350__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__10352),null);
}
} else {
var v = cljs.core.first.call(null,s__10350__$2);
return cljs.core.cons.call(null,(function (){
mrsudoku.model.solver.newStack = cljs.core.conj.call(null,stack,v);

if(cljs.core._EQ_.call(null,mrsudoku.model.solver.vIndex,(3))){
cljs.core.println.call(null,v,values);
} else {
}

return cljs.core.remove.call(null,cljs.core.nil_QMARK_,mrsudoku.model.solver.maximalMatchingRecursive.call(null,res,mrsudoku.model.solver.newStack,cljs.core.dissoc.call(null,values,mrsudoku.model.solver.vIndex),valuesSize,cljs.core.conj.call(null,test,cljs.core.PersistentArrayMap.createAsIfByAssoc([mrsudoku.model.solver.vIndex,v]))));
})()
,mrsudoku$model$solver$maximalMatchingRecursive_$_iter__10349.call(null,cljs.core.rest.call(null,s__10350__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,cljs.core.vec.call(null,mrsudoku.model.solver.vSet));
}
});
mrsudoku.model.solver.allMaximalMatchingOfDomain = (function mrsudoku$model$solver$allMaximalMatchingOfDomain(linkGraph){
mrsudoku.model.solver._v = cljs.core.get.call(null,linkGraph,(0));

mrsudoku.model.solver._kv = cljs.core.vec.call(null,cljs.core.keys.call(null,mrsudoku.model.solver._v));

mrsudoku.model.solver._c = cljs.core.get.call(null,linkGraph,(1));

return mrsudoku.model.solver.maximalMatchingRecursive.call(null,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentHashSet.EMPTY,mrsudoku.model.solver._v,cljs.core.count.call(null,mrsudoku.model.solver._v),cljs.core.PersistentVector.EMPTY);
});
mrsudoku.model.solver.printGrid = (function mrsudoku$model$solver$printGrid(grid){
var seq__10354 = cljs.core.seq.call(null,cljs.core.range.call(null,(1),(10)));
var chunk__10355 = null;
var count__10356 = (0);
var i__10357 = (0);
while(true){
if((i__10357 < count__10356)){
var x = cljs.core._nth.call(null,chunk__10355,i__10357);
cljs.core.println.call(null,cljs.core.reduce.call(null,((function (seq__10354,chunk__10355,count__10356,i__10357,x){
return (function (res,v){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(res),"  ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(((cljs.core._EQ_.call(null,(10),cljs.core.get.call(null,v,mrsudoku.model.solver.VALUE)))?"X":((cljs.core._EQ_.call(null,null,cljs.core.get.call(null,v,mrsudoku.model.solver.VALUE)))?".":cljs.core.get.call(null,v,mrsudoku.model.solver.VALUE))))].join('');
});})(seq__10354,chunk__10355,count__10356,i__10357,x))
,"",mrsudoku.model.grid.row.call(null,grid,x)));


var G__10358 = seq__10354;
var G__10359 = chunk__10355;
var G__10360 = count__10356;
var G__10361 = (i__10357 + (1));
seq__10354 = G__10358;
chunk__10355 = G__10359;
count__10356 = G__10360;
i__10357 = G__10361;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__10354);
if(temp__5720__auto__){
var seq__10354__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__10354__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__10354__$1);
var G__10362 = cljs.core.chunk_rest.call(null,seq__10354__$1);
var G__10363 = c__4351__auto__;
var G__10364 = cljs.core.count.call(null,c__4351__auto__);
var G__10365 = (0);
seq__10354 = G__10362;
chunk__10355 = G__10363;
count__10356 = G__10364;
i__10357 = G__10365;
continue;
} else {
var x = cljs.core.first.call(null,seq__10354__$1);
cljs.core.println.call(null,cljs.core.reduce.call(null,((function (seq__10354,chunk__10355,count__10356,i__10357,x,seq__10354__$1,temp__5720__auto__){
return (function (res,v){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(res),"  ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(((cljs.core._EQ_.call(null,(10),cljs.core.get.call(null,v,mrsudoku.model.solver.VALUE)))?"X":((cljs.core._EQ_.call(null,null,cljs.core.get.call(null,v,mrsudoku.model.solver.VALUE)))?".":cljs.core.get.call(null,v,mrsudoku.model.solver.VALUE))))].join('');
});})(seq__10354,chunk__10355,count__10356,i__10357,x,seq__10354__$1,temp__5720__auto__))
,"",mrsudoku.model.grid.row.call(null,grid,x)));


var G__10366 = cljs.core.next.call(null,seq__10354__$1);
var G__10367 = null;
var G__10368 = (0);
var G__10369 = (0);
seq__10354 = G__10366;
chunk__10355 = G__10367;
count__10356 = G__10368;
i__10357 = G__10369;
continue;
}
} else {
return null;
}
}
break;
}
});
mrsudoku.model.solver.applySolution = (function mrsudoku$model$solver$applySolution(grid,type,idOfDomain,solution){
return cljs.core.reduce.call(null,(function (res,s){
mrsudoku.model.solver.__case = cljs.core.get.call(null,s,(0));

mrsudoku.model.solver.__s = cljs.core.get.call(null,s,(1));

mrsudoku.model.solver.__posXY = mrsudoku.model.solver.getCasePositionFromDomainIds.call(null,type,idOfDomain,mrsudoku.model.solver.__case);

mrsudoku.model.solver.__x = cljs.core.get.call(null,mrsudoku.model.solver.__posXY,(0));

mrsudoku.model.solver.__y = cljs.core.get.call(null,mrsudoku.model.solver.__posXY,(1));

return mrsudoku.model.solver.changeOneValueInGrid.call(null,res,mrsudoku.model.solver.__x,mrsudoku.model.solver.__y,mrsudoku.model.solver.__s);
}),grid,solution);
});
mrsudoku.model.solver.validateSolution = (function mrsudoku$model$solver$validateSolution(solution){
if(((cljs.core._EQ_.call(null,cljs.core.PersistentArrayMap.EMPTY,solution)) || (cljs.core._EQ_.call(null,null,solution)))){
return false;
} else {
return cljs.core.reduce.call(null,(function (res,s){
if(cljs.core._EQ_.call(null,(10),cljs.core.get.call(null,s,(1)))){
return false;
} else {
return res;
}
}),true,solution);
}
});
mrsudoku.model.solver.backtrackingSolver = (function mrsudoku$model$solver$backtrackingSolver(grid,type,idOfDomain,solutions,idInSolution){
cljs.core.println.call(null,type,idOfDomain,cljs.core.count.call(null,solutions),idInSolution);

if(cljs.core._EQ_.call(null,(0),cljs.core.count.call(null,solutions))){
return null;
} else {
mrsudoku.model.solver.newGrid = mrsudoku.model.solver.applySolution.call(null,grid,type,idOfDomain,cljs.core.get.call(null,solutions,idInSolution));

cljs.core.println.call(null,solutions);

mrsudoku.model.solver.printGrid.call(null,mrsudoku.model.solver.newGrid);

if(cljs.core._EQ_.call(null,(9),idOfDomain)){
return mrsudoku.model.solver.newGrid;
} else {
mrsudoku.model.solver.newBacktrack = mrsudoku.model.solver.backtrackingSolver.call(null,mrsudoku.model.solver.newGrid,type,(idOfDomain + (1)),mrsudoku.model.solver.allMaximalMatchingOfDomain.call(null,mrsudoku.model.solver.buildLinkGraphOfDomain.call(null,mrsudoku.model.solver.newGrid,type,(idOfDomain + (1)))),(0));

if(cljs.core.not_EQ_.call(null,null,mrsudoku.model.solver.newBacktrack)){
return mrsudoku.model.solver.newBacktrack;
} else {
if((idInSolution >= cljs.core.count.call(null,solutions))){
return null;
} else {
return mrsudoku.model.solver.backtrackingSolver.call(null,mrsudoku.model.solver.newGrid,type,idOfDomain,solutions,(idInSolution + (1)));
}
}
}
}
});
mrsudoku.model.solver.testsolve = (function mrsudoku$model$solver$testsolve(grid){
mrsudoku.model.solver.solutions = mrsudoku.model.solver.allMaximalMatchingOfDomain.call(null,mrsudoku.model.solver.buildLinkGraphOfDomain.call(null,grid,mrsudoku.model.solver.ROW,(1)));

return mrsudoku.model.solver.backtrackingSolver.call(null,grid,mrsudoku.model.solver.ROW,(1),mrsudoku.model.solver.solutions,(0));
});
mrsudoku.model.solver.testprint = (function mrsudoku$model$solver$testprint(grid){
cljs.core.println.call(null,"############### TEST PRINT ###############");

cljs.core.println.call(null,"##########################################");

cljs.core.println.call(null,mrsudoku.model.solver.buildLinkGraphOfDomain.call(null,grid,mrsudoku.model.solver.ROW,(1)));

cljs.core.println.call(null,mrsudoku.model.solver.allMaximalMatchingOfDomain.call(null,mrsudoku.model.solver.buildLinkGraphOfDomain.call(null,grid,mrsudoku.model.solver.ROW,(1))));

cljs.core.println.call(null,cljs.core.get.call(null,mrsudoku.model.solver.allMaximalMatchingOfDomain.call(null,mrsudoku.model.solver.buildLinkGraphOfDomain.call(null,grid,mrsudoku.model.solver.ROW,(1))),(0)));

mrsudoku.model.solver.printGrid.call(null,mrsudoku.model.solver.applySolution.call(null,grid,mrsudoku.model.solver.ROW,(1),cljs.core.get.call(null,mrsudoku.model.solver.allMaximalMatchingOfDomain.call(null,mrsudoku.model.solver.buildLinkGraphOfDomain.call(null,grid,mrsudoku.model.solver.ROW,(1))),(0))));

return cljs.core.println.call(null,"##########################################");
});
/**
 * Solve the sudoku `grid` by returing a full solved grid,
 *  or `nil` if the solver fails.
 */
mrsudoku.model.solver.solve = (function mrsudoku$model$solver$solve(grid){
mrsudoku.model.solver.testprint.call(null,grid);

return null;
});

//# sourceMappingURL=solver.js.map
