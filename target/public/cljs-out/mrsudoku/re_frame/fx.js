// Compiled by ClojureScript 1.10.339 {}
goog.provide('re_frame.fx');
goog.require('cljs.core');
goog.require('re_frame.router');
goog.require('re_frame.db');
goog.require('re_frame.interceptor');
goog.require('re_frame.interop');
goog.require('re_frame.events');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
goog.require('re_frame.trace');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_(re_frame.registrar.kinds.call(null,re_frame.fx.kind))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
/**
 * Register the given effect `handler` for the given `id`.
 * 
 *   `id` is keyword, often namespaced.
 *   `handler` is a side-effecting function which takes a single argument and whose return
 *   value is ignored.
 * 
 *   Example Use
 *   -----------
 * 
 *   First, registration ... associate `:effect2` with a handler.
 * 
 *   (reg-fx
 *   :effect2
 *   (fn [value]
 *      ... do something side-effect-y))
 * 
 *   Then, later, if an event handler were to return this effects map ...
 * 
 *   {...
 * :effect2  [1 2]}
 * 
 * ... then the `handler` `fn` we registered previously, using `reg-fx`, will be
 * called with an argument of `[1 2]`.
 */
re_frame.fx.reg_fx = (function re_frame$fx$reg_fx(id,handler){
return re_frame.registrar.register_handler.call(null,re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.call(null,new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
if(re_frame.trace.is_trace_enabled_QMARK_.call(null)){
var _STAR_current_trace_STAR_11947 = re_frame.trace._STAR_current_trace_STAR_;
re_frame.trace._STAR_current_trace_STAR_ = re_frame.trace.start_trace.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));

try{try{var seq__11948 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__11949 = null;
var count__11950 = (0);
var i__11951 = (0);
while(true){
if((i__11951 < count__11950)){
var vec__11952 = cljs.core._nth.call(null,chunk__11949,i__11951);
var effect_key = cljs.core.nth.call(null,vec__11952,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11952,(1),null);
var temp__5718__auto___11968 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11968)){
var effect_fn_11969 = temp__5718__auto___11968;
effect_fn_11969.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11970 = seq__11948;
var G__11971 = chunk__11949;
var G__11972 = count__11950;
var G__11973 = (i__11951 + (1));
seq__11948 = G__11970;
chunk__11949 = G__11971;
count__11950 = G__11972;
i__11951 = G__11973;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__11948);
if(temp__5720__auto__){
var seq__11948__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11948__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__11948__$1);
var G__11974 = cljs.core.chunk_rest.call(null,seq__11948__$1);
var G__11975 = c__4351__auto__;
var G__11976 = cljs.core.count.call(null,c__4351__auto__);
var G__11977 = (0);
seq__11948 = G__11974;
chunk__11949 = G__11975;
count__11950 = G__11976;
i__11951 = G__11977;
continue;
} else {
var vec__11955 = cljs.core.first.call(null,seq__11948__$1);
var effect_key = cljs.core.nth.call(null,vec__11955,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11955,(1),null);
var temp__5718__auto___11978 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11978)){
var effect_fn_11979 = temp__5718__auto___11978;
effect_fn_11979.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11980 = cljs.core.next.call(null,seq__11948__$1);
var G__11981 = null;
var G__11982 = (0);
var G__11983 = (0);
seq__11948 = G__11980;
chunk__11949 = G__11981;
count__11950 = G__11982;
i__11951 = G__11983;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(re_frame.trace.is_trace_enabled_QMARK_.call(null)){
var end__11777__auto___11984 = re_frame.interop.now.call(null);
var duration__11778__auto___11985 = (end__11777__auto___11984 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.call(null,re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.call(null,re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__11778__auto___11985,new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now.call(null)));

re_frame.trace.run_tracing_callbacks_BANG_.call(null,end__11777__auto___11984);
} else {
}
}}finally {re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR_11947;
}} else {
var seq__11958 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__11959 = null;
var count__11960 = (0);
var i__11961 = (0);
while(true){
if((i__11961 < count__11960)){
var vec__11962 = cljs.core._nth.call(null,chunk__11959,i__11961);
var effect_key = cljs.core.nth.call(null,vec__11962,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11962,(1),null);
var temp__5718__auto___11986 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11986)){
var effect_fn_11987 = temp__5718__auto___11986;
effect_fn_11987.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11988 = seq__11958;
var G__11989 = chunk__11959;
var G__11990 = count__11960;
var G__11991 = (i__11961 + (1));
seq__11958 = G__11988;
chunk__11959 = G__11989;
count__11960 = G__11990;
i__11961 = G__11991;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__11958);
if(temp__5720__auto__){
var seq__11958__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__11958__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__11958__$1);
var G__11992 = cljs.core.chunk_rest.call(null,seq__11958__$1);
var G__11993 = c__4351__auto__;
var G__11994 = cljs.core.count.call(null,c__4351__auto__);
var G__11995 = (0);
seq__11958 = G__11992;
chunk__11959 = G__11993;
count__11960 = G__11994;
i__11961 = G__11995;
continue;
} else {
var vec__11965 = cljs.core.first.call(null,seq__11958__$1);
var effect_key = cljs.core.nth.call(null,vec__11965,(0),null);
var effect_value = cljs.core.nth.call(null,vec__11965,(1),null);
var temp__5718__auto___11996 = re_frame.registrar.get_handler.call(null,re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5718__auto___11996)){
var effect_fn_11997 = temp__5718__auto___11996;
effect_fn_11997.call(null,effect_value);
} else {
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: no handler registered for effect:",effect_key,". Ignoring.");
}


var G__11998 = cljs.core.next.call(null,seq__11958__$1);
var G__11999 = null;
var G__12000 = (0);
var G__12001 = (0);
seq__11958 = G__11998;
chunk__11959 = G__11999;
count__11960 = G__12000;
i__11961 = G__12001;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
var seq__12002 = cljs.core.seq.call(null,value);
var chunk__12003 = null;
var count__12004 = (0);
var i__12005 = (0);
while(true){
if((i__12005 < count__12004)){
var map__12006 = cljs.core._nth.call(null,chunk__12003,i__12005);
var map__12006__$1 = ((((!((map__12006 == null)))?(((((map__12006.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__12006.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__12006):map__12006);
var effect = map__12006__$1;
var ms = cljs.core.get.call(null,map__12006__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__12006__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number')))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__12002,chunk__12003,count__12004,i__12005,map__12006,map__12006__$1,effect,ms,dispatch){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__12002,chunk__12003,count__12004,i__12005,map__12006,map__12006__$1,effect,ms,dispatch))
,ms);
}


var G__12010 = seq__12002;
var G__12011 = chunk__12003;
var G__12012 = count__12004;
var G__12013 = (i__12005 + (1));
seq__12002 = G__12010;
chunk__12003 = G__12011;
count__12004 = G__12012;
i__12005 = G__12013;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__12002);
if(temp__5720__auto__){
var seq__12002__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12002__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__12002__$1);
var G__12014 = cljs.core.chunk_rest.call(null,seq__12002__$1);
var G__12015 = c__4351__auto__;
var G__12016 = cljs.core.count.call(null,c__4351__auto__);
var G__12017 = (0);
seq__12002 = G__12014;
chunk__12003 = G__12015;
count__12004 = G__12016;
i__12005 = G__12017;
continue;
} else {
var map__12008 = cljs.core.first.call(null,seq__12002__$1);
var map__12008__$1 = ((((!((map__12008 == null)))?(((((map__12008.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__12008.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__12008):map__12008);
var effect = map__12008__$1;
var ms = cljs.core.get.call(null,map__12008__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.call(null,map__12008__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_.call(null,dispatch)) || (!(typeof ms === 'number')))){
re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-later value:",effect);
} else {
re_frame.interop.set_timeout_BANG_.call(null,((function (seq__12002,chunk__12003,count__12004,i__12005,map__12008,map__12008__$1,effect,ms,dispatch,seq__12002__$1,temp__5720__auto__){
return (function (){
return re_frame.router.dispatch.call(null,dispatch);
});})(seq__12002,chunk__12003,count__12004,i__12005,map__12008,map__12008__$1,effect,ms,dispatch,seq__12002__$1,temp__5720__auto__))
,ms);
}


var G__12018 = cljs.core.next.call(null,seq__12002__$1);
var G__12019 = null;
var G__12020 = (0);
var G__12021 = (0);
seq__12002 = G__12018;
chunk__12003 = G__12019;
count__12004 = G__12020;
i__12005 = G__12021;
continue;
}
} else {
return null;
}
}
break;
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if(!(cljs.core.vector_QMARK_.call(null,value))){
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value);
} else {
return re_frame.router.dispatch.call(null,value);
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if(!(cljs.core.sequential_QMARK_.call(null,value))){
return re_frame.loggers.console.call(null,new cljs.core.Keyword(null,"error","error",-978969032),"re-frame: ignoring bad :dispatch-n value. Expected a collection, got got:",value);
} else {
var seq__12022 = cljs.core.seq.call(null,cljs.core.remove.call(null,cljs.core.nil_QMARK_,value));
var chunk__12023 = null;
var count__12024 = (0);
var i__12025 = (0);
while(true){
if((i__12025 < count__12024)){
var event = cljs.core._nth.call(null,chunk__12023,i__12025);
re_frame.router.dispatch.call(null,event);


var G__12026 = seq__12022;
var G__12027 = chunk__12023;
var G__12028 = count__12024;
var G__12029 = (i__12025 + (1));
seq__12022 = G__12026;
chunk__12023 = G__12027;
count__12024 = G__12028;
i__12025 = G__12029;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__12022);
if(temp__5720__auto__){
var seq__12022__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12022__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__12022__$1);
var G__12030 = cljs.core.chunk_rest.call(null,seq__12022__$1);
var G__12031 = c__4351__auto__;
var G__12032 = cljs.core.count.call(null,c__4351__auto__);
var G__12033 = (0);
seq__12022 = G__12030;
chunk__12023 = G__12031;
count__12024 = G__12032;
i__12025 = G__12033;
continue;
} else {
var event = cljs.core.first.call(null,seq__12022__$1);
re_frame.router.dispatch.call(null,event);


var G__12034 = cljs.core.next.call(null,seq__12022__$1);
var G__12035 = null;
var G__12036 = (0);
var G__12037 = (0);
seq__12022 = G__12034;
chunk__12023 = G__12035;
count__12024 = G__12036;
i__12025 = G__12037;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.call(null,re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_.call(null,value)){
var seq__12038 = cljs.core.seq.call(null,value);
var chunk__12039 = null;
var count__12040 = (0);
var i__12041 = (0);
while(true){
if((i__12041 < count__12040)){
var event = cljs.core._nth.call(null,chunk__12039,i__12041);
clear_event.call(null,event);


var G__12042 = seq__12038;
var G__12043 = chunk__12039;
var G__12044 = count__12040;
var G__12045 = (i__12041 + (1));
seq__12038 = G__12042;
chunk__12039 = G__12043;
count__12040 = G__12044;
i__12041 = G__12045;
continue;
} else {
var temp__5720__auto__ = cljs.core.seq.call(null,seq__12038);
if(temp__5720__auto__){
var seq__12038__$1 = temp__5720__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__12038__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__12038__$1);
var G__12046 = cljs.core.chunk_rest.call(null,seq__12038__$1);
var G__12047 = c__4351__auto__;
var G__12048 = cljs.core.count.call(null,c__4351__auto__);
var G__12049 = (0);
seq__12038 = G__12046;
chunk__12039 = G__12047;
count__12040 = G__12048;
i__12041 = G__12049;
continue;
} else {
var event = cljs.core.first.call(null,seq__12038__$1);
clear_event.call(null,event);


var G__12050 = cljs.core.next.call(null,seq__12038__$1);
var G__12051 = null;
var G__12052 = (0);
var G__12053 = (0);
seq__12038 = G__12050;
chunk__12039 = G__12051;
count__12040 = G__12052;
i__12041 = G__12053;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return clear_event.call(null,value);
}
}));
re_frame.fx.reg_fx.call(null,new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if(!((cljs.core.deref.call(null,re_frame.db.app_db) === value))){
return cljs.core.reset_BANG_.call(null,re_frame.db.app_db,value);
} else {
return null;
}
}));

//# sourceMappingURL=fx.js.map
