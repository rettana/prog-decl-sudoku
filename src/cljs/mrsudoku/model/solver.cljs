;; Solver of Sudoku
(ns mrsudoku.model.solver
  (:require [mrsudoku.model.grid :as g]
            [mrsudoku.model.conflict :as c]))

;; Some macro values
(def VALUE :value)
(def COL :col)
(def ROW :row)
(def BLOCK :block)


;; ######################################################################################################################################
;; ########################################################## MAXIMAL MATCHING ##########################################################
;; ######################################################################################################################################
(defn getDomain [grid type idOfDomain]
  (if (= type COL)
    (g/col grid idOfDomain)
    (if (= type ROW)
      (g/row grid idOfDomain)
      (if (= type BLOCK)
        (g/block grid idOfDomain)
        )
      )
    )
  )

;; Return the set of possible values for a domain
;; type : type of the domain (col,row,block)
;; idOfDomain : Index of the domain
(defn getPossiblesValuesOfDomain [grid type idOfDomain]
  "Return a set of possible values"
  (def allValues (set(for [i (range 1 10)]
                       i)))
  (def valuesOfDomain (set (remove nil? (reduce (fn [res x] (conj res (get x VALUE)))  #{} (getDomain grid type idOfDomain)))))
  (clojure.set/difference allValues valuesOfDomain)
  )

;; Return the set of empty case from a domain
;; type : type of the domain (col,row,block)
;; idOfDomain : Index of the domain
(defn getEmptyCasesIndexOfDomain [grid type idOfDomain]
  (set(remove nil? (for [i (range 0 9)]
                     (if (= (get (get (getDomain grid type idOfDomain) i) :value ) nil)
                       (+ i 1)
                       )
                     )))
  )

;; Return the index of the tab as shown below :
;; # Sudoku :
;; 0  1  2  | 3  4  5  | 6  7  8
;; 9  10 11 | 12 13 14 | 15 16 17
;; 18 19 20 | 21 22 23 | 24 25 26
;; ------------------------------
;; 27 28 29 | 30 31 32 | 34 35 36
;; 36 37 38 | 39 40 41 | 42 43 44
;; 45 46 47 | 48 49 50 | 51 52 53
;; ------------------------------
;; 54 55 56 | 57 58 59 | 60 61 62
;; 63 64 65 | 66 67 68 | 69 70 71
;; 72 73 74 | 75 76 77 | 78 79 80
;;
;; type : type of the domain (col,row,block)
;; idOfDomain : index of the domain (1-9)
;; idInDomain : index in the domain (1-9)
(defn getCaseIdOfTab [type idOfDomain idInDomain]
  (def p (- idOfDomain 1))
  (def i ( - idInDomain 1))
  (if (= type ROW)
    (+ (* p 9) i)
    (if (= type COL)
      (+ (* p 9) i)
      (if (= type BLOCK)
        (do
          (+
           ;; first id calculation with idOfDomain
           (* 3 (rem p 3)) ;; horizontal calculation
           (* 27 (quot p 3)) ;; vertical calculation
           ;; calculation of the id from the starting id with idInDomain
           (rem i 3) ;; horizontal calculation
           (* 9 (quot i 3)) ;; vertical calculation
           )
          )
        )
      )
    )
  )

;; Return a position [x y] from the id of the tab from getCaseIdOfTab
(defn getCasePositionFromIdOfTab [idOfTab]
  ;;(println "idoftab = " idOfTab)
  [
    (+ 1 (rem idOfTab 9))
    (+ 1 (quot idOfTab 9))
    ]
  )

;; Return a position [x y] from domains Ids
;; type : type of the domain (col,row,block)
;; idOfDomain : index of the domain (1-9)
;; idInDomain : index in the domain (1-9)
(defn getCasePositionFromDomainIds [type idOfDomain idInDomain]
  (getCasePositionFromIdOfTab (getCaseIdOfTab type idOfDomain idInDomain))
  )

;; Return the id of the block from a position [x y]
(defn getBlockFromPosition [x y]
  (+ 1 (* 3 (quot (- y 1) 3)) (quot (- x 1)  3))
  )

;; Return a grid with one case with a modified value
;; grid : initial grid
;; x : horizontal position of the case
;; y : vertical position of the case
;; value : new value to assign to the case
(defn changeOneValueInGrid [grid x y value]
  (def newCell (assoc (assoc  (g/cell grid x y) :value value ) :status :set))
  (g/change-cell grid x y newCell)

  )

;; Return all the possibles values for one case, comparing all other domain with the value
;; grid : initial grid
;; possibleValue : Possibles values of the domain
;; type : type of the domain (col,row,block)
;; idOfDomain : index of the domain (1-9)
;; idInDomain : index in the domain (1-9)
(defn getPossiblesValuesOfCase [grid possibleValues type idOfDomain idInDomain]
  {idInDomain (set (remove nil? (for [v possibleValues]
    (do
      (def posXY (getCasePositionFromDomainIds type idOfDomain idInDomain))
      (def x (get posXY 0))
      (def y (get posXY 1))
      (def tmpGrid (changeOneValueInGrid grid x y v ))
      (def tmpCol (g/col tmpGrid x))
      (def tmpRow (g/row tmpGrid y))
      (def tmpBlock (g/block tmpGrid (getBlockFromPosition x y)))
      ;;(println x y  idOfDomain idInDomain v (getBlockFromPosition x y) (c/block-conflicts tmpBlock v) (c/row-conflicts tmpRow v) (c/col-conflicts tmpCol v))
      (if (= {} (c/block-conflicts tmpBlock v))
        (if (= {} (c/row-conflicts tmpRow v))
          (if (= {} (c/col-conflicts tmpCol v))
            v
            )
          )
        )
      )
    )))}
  )

;; Reverse function of getPossibleValuesOfCase
(defn getPossiblesCaseOfValues [setOfPossiblesValuesOfCase idInDomain]
  (reduce
   (fn [res v] (assoc res v #{idInDomain}))
   { }
   setOfPossiblesValuesOfCase
    )
  )


;; Return one possible solution, taking the solution with the less of edge in priority
;; For example : possible values = #{1 2 4} | Possibles cases by value : {1 #{3 7}, 2 #{3 5 6 7}, 4 #{3 5 6}, 7 #{5 6}}
;; Will return  7 because 7 has the lesser possible cases
(defn solveOneCase [domainVals caseOFValues idCase]
  (reduce
   (fn [minIndex v]
     (if (< (count (get caseOFValues v))
            (if (= minIndex 10) 10 (count (get caseOFValues minIndex)))
            )
       v
       minIndex
       )
     )
   10 ;; index can't be higher than 10
   (vec (get domainVals idCase))
    )
  )

;; Return the element with the smaller size from a map of list
(defn IndexOfCaseWithMinimalSize [domainVals]
  (reduce (fn [res i]
            (if (< (count (get i 1 )) (if (= res 10) 10 (count (get domainVals res))))
              (get i 0)
              res)
            )
          10
          domainVals)
  )

;; Return a biparti graph with the links between them as set for each nodes / values
;; Format of the return value is [ linksFromEmptyCase , linksFromValues ]
;; example taking as a base domain block 1 of the sudoku :
;; | 5 3 . | Empty cases      : #{ 3 5 6 7 } |
;; | 6 . . | Possibles values : #{ 1 2 4 7 } |
;; | . 9 8 |                                 |
;;
;; Graph inducted represented as a matrix (after checking all the conflicts with other domains) :
;;    | 1 2 4 7 | horizontal are possibles values
;;    |‾‾‾‾‾‾‾‾‾| vertical are empty cases
;; 3  | 1 1 1 0 | 1 = link present between the empty case and the value
;; 5  | 0 1 1 1 | 0 = no link present between the empty case and the value
;; 6  | 0 1 1 1 |
;; 7  | 1 1 0 0 |
;;
;; Return value is :
;; [{3 #{1 2 4}, 5 #{2 4 7}, 6 #{2 4 7}, 7 #{1 2}},
;; {1 #{3 7}, 2 #{3 5 6 7}, 4 #{3 5 6}, 7 #{5 6}}]
(defn buildLinkGraphOfDomain [grid type idOfDomain]
  (def _possiblesVals (getPossiblesValuesOfDomain grid type idOfDomain))
  (def _emptyCases (getEmptyCasesIndexOfDomain grid type idOfDomain))
  ;;(println _possiblesVals _emptyCases)
  ;;Getting all possibles values of cases in the domain
  (def _domainVals(reduce
   (fn [res c] (conj res (getPossiblesValuesOfCase grid _possiblesVals type idOfDomain c)))
                   {}
                    _emptyCases
    ))
  ;;Getting all possible cases of values
  (def _casesOfValues(reduce
   (fn [res p] (merge-with into res p ))
   {}
    (for [i (keys _domainVals)]
    (getPossiblesCaseOfValues  (get _domainVals i) i)
    )))
  [_domainVals _casesOfValues]
  )

;; Return a graph whithout the couple [idCase idVal].
;; Removes completely the entries containing either idCase or idVal
(defn removeOneCoupleOfLinkGraph [linkGraph idCase idVal]
  (def tmpCase (dissoc (reduce (fn [res k] (assoc res k (disj (get res k) idCase) )) (get linkGraph 1) (keys (get linkGraph 1))) idVal ))
  (def tmpVal (dissoc (reduce (fn [res k] (assoc res k (disj (get res k) idVal) )) (get linkGraph 0) (keys (get linkGraph 0))) idCase))
  [tmpVal tmpCase]
  )

;; Return the maximal matching ( = solving ) of the graph
;; Process to find the maximal matching is :
;; 1. Make the biparti graph of links (fn : buildLinkGraphOfDomain) => [ emtpyCaseLink , valuesLink ]
;; 2. Take the smallest entry in emptyCaseLink (fn : IndexOfCaseWithMinimalSize)
;; 3. From the smallest entry in emptyCaseLink, take the value which has the smallest size in valuesLink.
;;    This value is the solution of the case (fn: solveOneCase)
;; 4. Do (2-3) again from a graph of link whithout the couple [smallestEmptyCase, solution] (fn : removeOneCoupleOfLinkGraph and this function)
(defn solveLinkGraph [linkGraph res]
  (if (not= {} (get linkGraph 0))
    (do
      (def minI (IndexOfCaseWithMinimalSize (get linkGraph 0)))
      (def subRes (solveOneCase (get linkGraph 0) (get linkGraph 1) minI))
      ;;(println "####" minI subRes)(println (get linkGraph 0))(println (get linkGraph 1))
      (merge
       (assoc res minI subRes)
       (solveLinkGraph (removeOneCoupleOfLinkGraph linkGraph minI subRes) res)
       )
      )
    res
    )
  )

;; Remove edges from a map { leftLink1 rightLink1 , leftLink2 rightLink2 , ... } to a graph of links and return the graph of link whithout these edges
(defn removeEdgesFromMap [linkGraph map]
  (reduce
   (fn [res m]
     (do
       (def l (get  m 0)) ;; left value of the edge
       (def r (get  m 1)) ;; right value of the edge
       (def __vals (get res 0))
       (def __cases (get res 1))
       [
         (if (= 1 (count (get __vals l))) __vals (assoc __vals l (disj (get __vals l) r)))
         (if (= 1 (count (get __cases r))) __cases (assoc __cases r (disj (get __cases r) l)))
         ]
       )
     )
   linkGraph
   map
    )
  )

;; works like removeEdgesFromMap but removes only the first edge
(defn removeOneEdgeFromMap [linkGraph map]
  (if (= {} map )
    nil
    (do
      (def vMap (get (vec map) 0))
      (def ___l (get vMap 0)) ;; left value of the edge
      (def ___r (get vMap 1)) ;; right value of the edge
      (def ___vals (get linkGraph 0))
      (def ___cases (get linkGraph 1))
      (if (or (= 1 (count (get ___vals ___l))) (= 1 (count (get ___cases ___r)))  )
        (removeOneEdgeFromMap linkGraph (dissoc map ___l))
        (removeEdgesFromMap linkGraph {___l ___r})
        )
      )
    )
  )

;; Find a Maximal Matching of a domain of a grid
;; check solveLinkGraph for detailled process.
;; return a map of solution {case1 solution1, case2 solution2, ...}
(defn maximalMatchingOfDomain [linkGraph]
  (def _vals (get linkGraph 0))
  (def _cases (get linkGraph 1))
  ;;(println _vals _cases)
  (solveLinkGraph  linkGraph {})
  )

;; take keys [k1 k2 ...] and a set #{s1 s2 ...} and transform it to a map { k1 s1, s2 s2, ...}
(defn keyAndSetToMap [keys _set]
  (reduce (fn [res i]
            (assoc res (get keys i) (get (vec _set) i))
            )
          nil
          (range (count keys))
          )
  )


;; Create a list of maximal matching by recursion
;; The function look at all possibles combinaison possibles and keep the one which have the maximal size
;; this function doesn't work and can't figure why it doesn't work.
;; This function test each possibilities in one case and call recursively the other one (by giving the same list of values, minus the actual one)
;; unfortunately, the removed one comes back on later recursive calls...
;; ie : { 3 #{  1 2 4 }, 6 #{ 2 4 7 }, 7 #{2 4 7}, 9#{ 2 7 }}
;; the fcuntion will test each values possibles  and look like this:
;; 3 - 1  (list = { 3 #{  1 2 4 }, 6 #{ 2 4 7 }, 7 #{2 4 7}, 9#{ 2 7 }})
;; 6 - 2  (list = { 6 #{ 2 4 7 }, 7 #{2 4 7}, 9#{ 2 7 }}
;; 7 - 2  (list = { 7 #{2 4 7}, 9#{ 2 7 }}
;; 9 - 2  (list = {9#{ 2 7 }}
;; 9 - 7  (list = {9#{ 2 7 }}
;; After this point I don't know why but it comes back to already read value and list becomes weird...
;; 7 - 2 (list = { 3 #{  1 2 4 },  7 #{2 4 7}, 9#{ 2 7 })
;; [...]
;; In the end It gives a partial list of maximal matching.
;;
;; res : list of maximal matching
;; stack : set of values to check if the maximal matching is valid
;; values : list of values (first element of linkGraph)
;; valuesSize : = (count values) on the first call
;; test : added later but contains the result in form {case value} which is usable on applySolution
;;        It prevents missing cases values due to array movements
(defn maximalMatchingRecursive [res stack values valuesSize test ]
  (if (= {} values)
    (if (= (count stack) valuesSize)
      (do
        (conj res  test)
        )
      )
    (do
      (def vIndex (get (get (vec values) 0)0))
      (def vSet (get values vIndex))
      (for [v (vec vSet)]
        (do
          (def newStack (conj stack v))
            (do
              (if (= vIndex 3 ) (println v values))
              (remove nil? (maximalMatchingRecursive res newStack (dissoc values vIndex) valuesSize (conj test {vIndex v}))))
          )
        )


      )
    )
  )

;;Return a list of maximal matching for a linkGraph (= a domain)
(defn allMaximalMatchingOfDomain [linkGraph]
  (def _v (get linkGraph 0))
  (def _kv (vec (keys _v)))
  (def _c (get linkGraph 1))
  (maximalMatchingRecursive
    []
   #{}
   _v
   (count _v)
    []
    )

  )

;; ######################################################################################################################################
;; ############################################################# MAIN SOLVER ############################################################
;; ######################################################################################################################################

;; print a readable sudoku grid on terminal
;; could not find the function which is already doing that so I coded it again
(defn printGrid [grid]
  (doseq [x (range 1 10)]
    (println (reduce (fn [res v] (str res "  " (if (= 10 (get v VALUE)) "X" ( if (= nil (get v VALUE)) "."  (get v VALUE)))) ) "" (g/row grid x)))
    )
  )

;; Apply each solution to the grid
;; grid : base grid
;; type : type of the domain (block,row,col)
;; solutions : maximal matching of the domain
(defn applySolution [grid type idOfDomain solution]
  (reduce
   (fn [res s]
     (def __case (get s 0))
     (def __s (get s 1))
     (def __posXY (getCasePositionFromDomainIds type idOfDomain __case))
     (def __x (get __posXY 0))
     (def __y (get __posXY 1))
     ;;(println s __posXY)
     (changeOneValueInGrid res __x __y __s)
     )
    grid
   solution
    )
  )

;; Search for any 10 in the solution array. Return false if there is one or more 10
;; solutions : maximal matching of a domain
(defn validateSolution [solution]
  (if (or (= {} solution) (= nil solution))
    false
    (reduce
     (fn [res s] (if (= 10 (get s 1) ) false res))
     true
     solution
            )
    )
  )

;; Doesn't work because allMaximalDomain doesn't work. I think all other functions works.
;; Return a solved grid by using backtracking.
;; This function backtrack only on one domain :
;; If "block" is choosed, it will continue on the next block only and wont change of domain type
;; this function could be optimized by mixing up domain, by choosing adequate domains to solve before others.
;; grid : a non solved grid
;; type : type of the domain (block,row,col)
;; idOfDomain : actual id of the domain
;; solutions : a list of maximal matching
;; idInSolution : actual id in the solutions list
(defn backtrackingSolver [grid type idOfDomain solutions idInSolution ]
  (println type idOfDomain (count solutions) idInSolution)
  (if (= 0 (count solutions))
    nil ;; base case
    (do ;; if solutions exist
      (def newGrid (applySolution grid type idOfDomain (get solutions idInSolution)))
      (println solutions)
      (printGrid newGrid)
      (if (= 9 idOfDomain) ;; if = 9 end of solver
      newGrid ;; return the solved grid
      (do ;; if not = 9, either backtrack or try the next solution (if there are other solutions)
        (def newBacktrack (backtrackingSolver
                           newGrid
                           type
                           (inc idOfDomain)
                           (allMaximalMatchingOfDomain (buildLinkGraphOfDomain newGrid type (inc idOfDomain)) )
                           0
                            ))
        (if (not= nil newBacktrack)
          newBacktrack
          (if (>= idInSolution (count solutions))
            nil
            (backtrackingSolver
            newGrid type idOfDomain solutions (inc idInSolution)
            )
            )
          )
        )
      ))
    )
  )


;; return a solved grid with defined parameter (solve by ROW in this function)
(defn testsolve [grid]
  (def solutions (allMaximalMatchingOfDomain (buildLinkGraphOfDomain grid ROW 1)))
  (backtrackingSolver grid ROW 1 solutions 0)
  )

;; Some prints to see if all was working fine.
(defn testprint
  [grid]
  (println "############### TEST PRINT ###############")

  (println "##########################################")
  (println (buildLinkGraphOfDomain grid ROW 1))
  (println (allMaximalMatchingOfDomain (buildLinkGraphOfDomain grid ROW 1)))
  (println (get(allMaximalMatchingOfDomain (buildLinkGraphOfDomain grid ROW 1))0))
  (printGrid (applySolution grid ROW 1 (get (allMaximalMatchingOfDomain(buildLinkGraphOfDomain grid ROW 1))0)))
  (println "##########################################")
  ;;(printGrid (testsolve grid))

  )

(defn solve
  "Solve the sudoku `grid` by returing a full solved grid,
 or `nil` if the solver fails."
  [grid]
  (testsolve grid)
  nil)
